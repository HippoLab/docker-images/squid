squid
======

Docker Image containing squid - TCP and HTTP proxy server

# Contents
- [Requirements](#requirements)
- [Building](#building)
- [Usage](#usage)
- [Licence](#licence)
- [Author Information](#author)

## <a name="requirements"></a> Requirements
Image to be built and run requires docker - containerization platform. Check [upstream documentation](https://docs.docker.com/get-docker/)
for how to install docker on your system

## <a name="building"></a> Building
To build an image run following command in repository root path
```sh
docker build --pull --tag squid .
```

## <a name="usage"></a> Usage
Environment variables is the main mechanism of manipulating application settings inside a docker container. Refer to
[upstream documentation](https://docs.docker.com/engine/reference/commandline/run/) for how to pass environment
variables to inside a container

### Environment variables
Currently, image recognizes following environment variables:

| Variable                  | Default value | Description                                         |
|---------------------------|---------------|-----------------------------------------------------|
| SQUID_HTTP_PORT           | 3128          | TCP port number squid to listen on                  |
| SQUID_ALLOWED_NETWORKS    | all           | Space separated list of allowed source networks     |
| SQUID_ALLOWED_DOMAINS     | all           | Space separated list of allowed destination domains |
| SQUID_ALLOWED_HTTP_PORTS  |               | Space separated list of allowed HTTP ports          |
| SQUID_ALLOWED_HTTPS_PORTS | 443           | Space separated list of allowed HTTPS ports         |

## <a name="license"></a> License
The module is distributed under [MIT Licence](LICENCE.txt). Please make sure you have read, understood and agreed to it
terms and conditions

## <a name="author"></a> Author Information
Vladimir Tiukhtin <vladimir.tiuhtin@hippolab.ru><br/>London
