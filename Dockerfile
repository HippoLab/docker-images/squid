FROM alpine:3.15 as builder

ARG SQUID_VERSION=5.3
ARG CONFD_VERSION=0.16.0

RUN wget -qO /usr/local/bin/confd https://github.com/kelseyhightower/confd/releases/download/v${CONFD_VERSION}/confd-${CONFD_VERSION}-linux-amd64 && \
    chmod +x /usr/local/bin/confd

WORKDIR /tmp/squid

RUN apk add --no-cache gcc g++ make musl-dev tar perl file

RUN wget -qO- http://www.squid-cache.org/Versions/v5/squid-${SQUID_VERSION}.tar.gz | tar -xz --strip=1

RUN ./configure --prefix=/usr/local --sysconfdir=/etc/squid \
      --build=x86_64-alpine-linux-musl \
      --enable-log-daemon-helpers=file \
      --disable-arch-native \
      --disable-loadable-modules \
      --disable-wccp \
      --disable-wccpv2 \
      --disable-snmp \
      --disable-eui \
      --disable-htcp \
      --disable-auth && \
    make && make install-strip

FROM alpine:3.15

COPY --from=builder /usr/local/ /usr/local/
COPY --from=builder /etc/squid/ /etc/squid/
COPY entrypoint.sh /entrypoint.sh
COPY confd /etc/confd

RUN apk add --no-cache ca-certificates libgcc libstdc++ && \
    chown -R squid:squid /etc/squid && \
    chmod +x /entrypoint.sh

USER squid:squid

LABEL name="Squid Proxy" \
      description="Squid is a caching proxy for the Web supporting HTTP, HTTPS, FTP, and more" \
      maintainer="Vladimir Tiukhtin <vladimir.tiukhtin@hippolab.ru>"

ENTRYPOINT ["/entrypoint.sh"]

CMD ["squid", "-N"]

