#!/bin/sh

init () {
  if [[ -z "${SQUID_ALLOWED_SSLPORTS}" ]] ## Allow HTTPS by default
    then export SQUID_ALLOWED_SSLPORTS=443
  fi
  confd -onetime -backend env
}

case $1 in
  'squid')
    init
    exec $@
  ;;
  *)
    $@
  ;;
esac
